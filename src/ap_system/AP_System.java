
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package ap_system;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.util.Scanner;

/**
 *
 * @author M_rey3000
 */
public class AP_System {
    
    private static final Scanner scan = new Scanner(System.in);
    private final String studentsNames = "Rollcall\\Students Names.txt";
    private final String rollCall = "Rollcall\\Rollcall.txt";
    public static void main(String[] args) throws IOException {
        AP_System m = new AP_System();
        
        int n;

        while (true) {
            n = showMainMenu();

            switch (n) {
            case 1 :
                m.rollCall();
                break;

            case 2 :
                m.showRollCall();
                break;

            case 3 :
                m.getNumberofPresent();
                break;

            default :
                break;
            }

            if (n == 0) {
                break;
            }
        }

        scan.close();
    }

    private void rollCall() throws FileNotFoundException, IOException {
        FileReader reader = new FileReader(new File(studentsNames));
        BufferedReader br = new BufferedReader(reader);
        FileWriter writer = new FileWriter(rollCall, true);

        try (BufferedWriter bw = new BufferedWriter(writer);) {
            int i = 1;
            int j = 1;

            System.out.println("1 = Present\n");

            while (br.ready()) {
                String name = i + ": " + br.readLine();

                System.out.println(name);

                int nn = scan.nextInt();

                if (nn == 1) {
                    bw.append(j + " | " + name);
                    bw.newLine();
                    j++;
                }

                i++;
            }

            bw.newLine();
            bw.write("------------------------------");
            bw.newLine();
            bw.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static int showMainMenu() {
        System.out.println("\n****************");
        System.out.println(">>> 1 :RollCall");
        System.out.println(">>> 2 :Show RollCall");
        System.out.println(">>> 3 :Number of Present");
        System.out.println(">>> 0 to Exit");
        System.out.println("");

        int n = scan.nextInt();

        return n;
    }

    private void showRollCall() throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(
                new File(rollCall)));
        int lines = 0;

        while (br.ready()) {
            String str = br.readLine();

            System.out.println(str);

            if (str.startsWith("---")) {
                lines++;
            }
        }
        System.out.println("\n>>>Number of Weeks Past: " + lines);
    }

    private void getNumberofPresent() throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(
                new File(rollCall)));
        Scanner input = new Scanner(System.in);
        int count = 0;

        System.out.println("Student Name ?\n");

        String name = input.nextLine();

        while (br.ready()) {
            String str = br.readLine();

            if (str.toLowerCase().contains(name.toLowerCase())) {
                if (count == 0) {
                    System.out.println(str.substring(4));
                }

                count++;
            }
        }

        System.out.println("\n >>>Number of Present : " + count);
    }
}